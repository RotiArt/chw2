﻿//создаём библиотеки данных
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//задаём пространство имён
namespace ConsoleApp1

{
    class Program
    {
        static void Main(string[] args)
        {
            //Задаём необходимую строку
            string name;
            //Выводим в консоль надпись
            Console.WriteLine("Введите имя");
            //Заваём значение строке на которую ссылались
            name = Console.ReadLine();
            string age;
            Console.WriteLine("Введите возраст");
            age = Console.ReadLine();
            string growth;
            Console.WriteLine("Введите рост");
            growth = Console.ReadLine();
            //предметы
            string maths;
            Console.WriteLine("Оценка по математике");
            maths = Console.ReadLine();

            string history;
            Console.WriteLine("Оценка по истории");
            history = Console.ReadLine();

            string russian;
            Console.WriteLine("Оценка по русскому");
            russian = Console.ReadLine();

            //Преобразуем данные string в float
            float maths1 = float.Parse(maths);
            float history1 = float.Parse(history);
            float russian1 = float.Parse(russian);
            //Считаем среднее орифметическое
            float mean = (maths1 + history1 + russian1) / 3;

            ////Выводим полученные значения
                var centerX = 50;
                var centerY = 20;
                Console.SetCursorPosition(centerX, centerY);
            Console.WriteLine(
              $"Имя:{name,12}\n" +
              $"Возраст:{age,2}\n" +
              $"Рост:{growth,3}\n" +
              $"Средний балл:{mean,2}");
            Console.ReadKey();
        }
    }
}
